import 'dart:async';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

void main() {
  print('----------ENTERED main()');
  runApp(MyApp());
}

/*
  MyApp is the root widget of the application.
 */
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('----------BUILDING MyApp');

    return MaterialApp(
        title: 'Music App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: AllSongs());
  }
}

/*
  AllSongs displays a list of all songs in the database.
 */
class AllSongs extends StatefulWidget {
  final String title = 'All Songs';

  AllSongs({
    Key key,
  }) : super(key: key);

  @override
  _AllSongsState createState() => _AllSongsState();
}

class _AllSongsState extends State<AllSongs> {
  Future<List<Song>> _songs;

  @override
  Widget build(BuildContext context) {
    print('----------BUILDING _AllSongsState');
    _songs = SongDatabase.loadAllSongs();

    return FutureBuilder<List<Song>>(
      future: _songs,
      builder: (context, snapshot) {
        // Reusable AppBar
        AppBar appBar = AppBar(
          title: Text(widget.title),
        );

        // If songs are loaded, display them
        if (snapshot.hasData) {
          return Scaffold(
            appBar: appBar,
            body: ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.all(8),
                  child: Container(
                    height: 50,
                    color: Colors.blue[100],
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Material(
                            color: Colors.blue[100],
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SongDetail(song: snapshot.data[index])));
                              },
                              child: Text(
                                snapshot.data[index].title,
                                style: TextStyle(fontSize: 20),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  snapshot.data[index].duration
                                      .toString()
                                      .split('.')
                                      .first
                                      .padLeft(8, '0'),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Icon(Icons.play_arrow),
                              ]),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                Map<String, String> filePaths =
                    await FilePicker.getMultiFilePath(type: FileType.AUDIO);
                setState(() {
                  filePaths.forEach((name, path) => SongDatabase.insertSong(
                      new Song(
                          title: name,
                          artist: 'Unknown',
                          duration: new Duration(seconds: 0),
                          filePath: path,
                          fileName: name,
                          timesPlayed: 0)));
                });
              },
              tooltip: 'Add Songs',
              child: Icon(Icons.add),
            ),
          );
        }

        // If there was an error loading songs, display it
        else if (snapshot.hasError) {
          return Scaffold(
            appBar: appBar,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(snapshot.error.toString()),
                ],
              ),
            ),
          );
        }

        // By default, if songs aren't loaded yet, show a loading spinner
        return Scaffold(
          appBar: appBar,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
              ],
            ),
          ),
        );
      },
    );
  }
}

class SongDetail extends StatefulWidget {
  final Song song;

  SongDetail({Key key, @required this.song}) : super(key: key);

  @override
  _SongDetailState createState() => _SongDetailState();
}

class _SongDetailState extends State<SongDetail> {
  final titleController = TextEditingController();
  final artistController = TextEditingController();
  final durationController = TextEditingController();

  @override
  void initState() {
    super.initState();
    titleController.text = widget.song.title;
    artistController.text = widget.song.artist;
    durationController.text = widget.song.duration.inSeconds.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Song Details'),
      ),
      body: Column(
        children: <Widget>[
          Text('Title:'),
          TextField(
            controller: titleController,
          ),
          Text('Artist:'),
          TextField(
            controller: artistController,
          ),
          Text('Duration:'),
          TextField(
            controller: durationController,
          ),
          FlatButton(
            child: Text('Delete'),
            onPressed: () {
              SongDatabase.deleteSong(widget.song.id);
              Navigator.pop(context);
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Song song = new Song(
            id: widget.song.id,
            title: titleController.text,
            artist: artistController.text,
            filePath: widget.song.filePath,
            fileName: widget.song.fileName,
            duration: new Duration(seconds: int.parse(durationController.text)),
            timesPlayed: widget.song.timesPlayed,
          );
          SongDatabase.updateSong(song);
          Navigator.pop(context);
        },
        tooltip: 'Submit',
        child: Icon(Icons.send),
      ),
    );
  }
}

/*
  SongDatabase is a utility class for interacting with
  the embedded SQLite database.
 */
class SongDatabase {
  static Database _db;

  static Future _createTables(Database db, int version) async {
    await db.execute(
        'CREATE TABLE song(id INTEGER NOT NULL PRIMARY KEY, title TEXT NOT NULL, artist TEXT NOT NULL, duration INTEGER, filePath TEXT NOT NULL, fileName TEXT NOT NULL, timesPlayed INTEGER NOT NULL);');
    await db.execute(
        'CREATE TABLE playlist(id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL);');
    await db.execute(
        'CREATE TABLE songPlaylistMap(id INTEGER NOT NULL PRIMARY KEY, songId INTEGER NOT NULL, playlistId INTEGER NOT NULL, FOREIGN KEY (songId) REFERENCES song(id), FOREIGN KEY (playlistId) REFERENCES playlist(id));');
  }

  static Future<void> _initializeDatabase() async {
    if (_db != null) {
      return;
    }

    _db = await openDatabase(join(await getDatabasesPath(), 'music_app.db'),
        onCreate: _createTables, version: 1);

    print('----------DATABASE INITIALIZED');
  }

  // TODO
  static Future<List<Song>> loadAllSongs() async {
    print('----------LOADING ALL SONGS');

    await _initializeDatabase();

    // Query the data back out
    final List<Map> maps = await _db.query('song');

    return List.generate(maps.length, (i) {
      return Song(
        id: maps[i]['id'],
        title: maps[i]['title'],
        artist: maps[i]['artist'],
        filePath: maps[i]['filePath'],
        fileName: maps[i]['fileName'],
        duration: Duration(seconds: maps[i]['duration']),
        timesPlayed: maps[i]['timesPlayed'],
      );
    });
  }

  static void insertSong(Song song) async {
    await _db.insert('song', song.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    print('----------INSERTED' + song.toString());
  }

  static void updateSong(Song song) async {
    await _db.update('song', song.toMap(), where: "id = ?", whereArgs: [song.id],);

    print('----------updated' + song.toString());
  }

  static void deleteSong(int id) async {
    await _db.delete('song', where: "id = ?", whereArgs: [id]);

    print('----------DELETED' + id.toString());
  }
}

class Song {
  int id;
  String title;
  String artist;
  Duration duration;
  String filePath;
  String fileName;
  int timesPlayed = 0;

  Song({this.id, this.title, this.artist, this.duration, this.filePath, this.fileName, this.timesPlayed});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'artist': artist,
      'duration': duration.inSeconds,
      'filePath' : filePath,
      'fileName' : fileName,
      'timesPlayed': timesPlayed,
    };
  }
}
